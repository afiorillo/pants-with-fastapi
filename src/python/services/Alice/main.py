from fastapi import FastAPI
import uvicorn
import requests

from common.models import CallMessage
from services.Alice.settings import Settings

app = FastAPI()
settings = Settings()

@app.get("/")
async def ok():
    return requests.get('https://httpbin.org/get').json()


@app.get("/call-bob")
async def call_bob():
    return "ok"


@app.get("/be-called")
async def be_called():
    return "ok"


if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=5000, log_level="info")
