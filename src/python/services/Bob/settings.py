from common.basesettings import BaseSettings


class Settings(BaseSettings):
    SERVICE_NAME: str = "Bob"
    ALICE_URL: str = "http://127.0.0.1:5000"
