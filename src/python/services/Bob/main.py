from fastapi import FastAPI
import uvicorn

from common.models import CallMessage
from services.Bob.settings import Settings

app = FastAPI()
settings = Settings()


@app.get("/")
async def ok():
    return "ok"


@app.get("/call-alice")
async def call_alice():
    return "ok"


@app.get("/be-called")
async def be_called():
    return "ok"


if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=5001, log_level="info")
