# Pants <3 FastAPI

A simple demonstration that combines [Pants](https://www.pantsbuild.org/) with multiple [FastAPI](https://fastapi.tiangolo.com/) services.

## Setup

You should probably have `pyenv` installed, along with Python 3.9.
Currently Pants doesn't play nicely with 3.10 annoyingly.

I.e.

```bash
$ pyenv install 3.9.10
# and to check
$ ./pants --version
```

To package the `Charles` package you need `go` installed.
This was tested to work with `go1.21.1 linux/amd64`, but anything supporting `go mod` ought to do -- you will need to update `pants.toml` to specify the allowed Go version.

## Structure

The application code will be structured like:

```
- src
  - python/
    - common/ (shared by multiple services)
    - services/
      - Alice/ (depends on common)
      - Bob/ (depends on common)
    - end2end/ (depends on all the services to run workflow tests)
  - docker/
    - alice_server/ (a docker image based on the Alice server)
  - go/
    - Charles/ (an unrelated, but colocated service)
```

## Commands

To run the Alice or Bob service

```bash
$ ./pants run src/python/services/Alice/main.py
# or
$ ./pants run src/python/services/Bob/main.py
```

To build the services, for running later on (in a Docker container for instance)

```bash
$ ./pants package ::
# and then to run Alice
$ ./dist/src.python.services.Alice/alice-server.pex
# or Bob
$ ./dist/src.python.services.Bob/bob-server.pex
```

## IDE Integration

### First-party code

[This page](https://www.pantsbuild.org/docs/setting-up-an-ide) has details.

To expose the IDE (e.g. PyCharm and VSCode) to first-party sources, you should have a `.env` file in the repository root that defines a `PYTHONPATH`.
In particular, the `PYTHONPATH` should be a colon-delimited `:` string pointing to each of the source roots under Pants.
Running this script will generate/append the `.env` file for you.

```bash
$ ./scripts/expose_source_roots_to_ide.sh
```

With it, you should be able to do things like "Jump to Definition" across different parts of the Pants monorepo.

### Third-Party code

Pants does not use virtual environments in the conventional way.
To make it play nice with an IDE, you must export a virtual environment and then tell your IDE that's where 3rd party code lives.
The script

```bash
$ ./scripts/expose_library_roots_to_ide.sh
# essentially
$ ./pants export ::
```

After this you should set your IDE to look at the new virtual environment.
It is probably in `./dist/export/python/virtualenv/3.9.10/bin/python`.

## Extra Notes

After re-visiting this project a year and change later, I found the installation instructions using Pyenv to be lacking.
Not specifically due to Pants, but due to my new laptop's somewhat strange approach to Python multi-tenancy.
My solution to this was to instead use a virtualenv `python -m venv .venv && source .venv/bin/activate` which then guaranteed using a specific Python version while invoking pants.
