# Contributing

Want to improve this? Great!
This is a side project for me, for which I have allocated approximately 0 maintenance hours.
PRs are welcome and appreciated, and I apologize in advance if your great change takes a long time to merge.

## Code of Conduct

- No hate speech, racism, sexism, chauvanisms of basically any kind. It's a demo application meant to help developers understand a tool. There isn't room for any "real" code, why would there be room for problematic content?
- No spam. Just please, don't.
- I, Andrew Fiorillo, am the final arbitrator for whether something deserves getting merged to `origin/main`. I feel pretty easy going, but I don't have patience for the previous two points.
- Keep it fun.

## Process

Make a fork in Gitlab, do your changes.
If you want to share, make a MR and tag me.
If I don't respond within like a week or something, maybe PM me?
